// Avoid `console` errors in browsers that lack a console.
if (!(window.console && console.log)) {
    (function() {
        var noop = function() {};
        var methods = ['assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error', 'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log', 'markTimeline', 'profile', 'profileEnd', 'markTimeline', 'table', 'time', 'timeEnd', 'timeStamp', 'trace', 'warn'];
        var length = methods.length;
        var console = window.console = {};
        while (length--) {
            console[methods[length]] = noop;
        }
    }());
}

 /*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas. Dual MIT/BSD license */
/*! NOTE: If you're already including a window.matchMedia polyfill via Modernizr or otherwise, you don't need this part */
window.matchMedia=window.matchMedia||function(a){"use strict";var c,d=a.documentElement,e=d.firstElementChild||d.firstChild,f=a.createElement("body"),g=a.createElement("div");return g.id="mq-test-1",g.style.cssText="position:absolute;top:-100em",f.style.background="none",f.appendChild(g),function(a){return g.innerHTML='&shy;<style media="'+a+'"> #mq-test-1 { width: 42px; }</style>',d.insertBefore(f,e),c=42===g.offsetWidth,d.removeChild(f),{matches:c,media:a}}}(document);

/*global jQuery */
/*!
* FitText.js 1.2
*
* Copyright 2011, Dave Rupert http://daverupert.com
* Released under the WTFPL license
* http://sam.zoy.org/wtfpl/
*
* Date: Thu May 05 14:23:00 2011 -0600
*/

(function( $ ){

  $.fn.fitText = function( kompressor, options ) {

    // Setup options
    var compressor = kompressor || 1,
        settings = $.extend({
          'minFontSize' : Number.NEGATIVE_INFINITY,
          'maxFontSize' : Number.POSITIVE_INFINITY
        }, options);

    return this.each(function(){

      // Store the object
      var $this = $(this);

      // Resizer() resizes items based on the object width divided by the compressor * 10
      var resizer = function () {
        $this.css('font-size', Math.max(Math.min($this.width() / (compressor*10), parseFloat(settings.maxFontSize)), parseFloat(settings.minFontSize)));
      };

      // Call once to set.
      resizer();

      // Call on resize. Opera debounces their resize by default.
      $(window).on('resize.fittext orientationchange.fittext', resizer);

    });

  };

})( jQuery );




/* @copyright (c) Dmitry Sheiko http://www.dsheiko.com  */
(function(d){var d=d,a=d.document,c=d.screen,b=function(h){var g={startX:0,endX:0},f={moveHandler:function(l){},endHandler:function(l){},minLengthRatio:0.3},j=function(){return g.endX>g.startX?"prev":"next"},k=function(){var l=Math.ceil(c.width*h.minLengthRatio);return Math.abs(g.endX-g.startX)>l},e=function(){for(var l in f){if(f.hasOwnProperty(l)){h[l]||(h[l]=f[l])}}},i={touchStart:function(l){if(l.touches.length>0){g.startX=l.touches[0].pageX}},touchMove:function(l){if(l.touches.length>0){g.endX=l.touches[0].pageX;h.moveHandler(j(),k())}},touchEnd:function(l){var m=l.changedTouches||l.touches;if(m.length>0){g.endX=m[0].pageX;k()&&h.endHandler(j())}}};e();if(!a.addEventListener){return{on:function(){},off:function(){}}}return{on:function(){a.addEventListener("touchstart",i.touchStart,false);a.addEventListener("touchmove",i.touchMove,false);a.addEventListener("touchend",i.touchEnd,false)},off:function(){a.removeEventListener("touchstart",i.touchStart);a.removeEventListener("touchmove",i.touchMove);a.removeEventListener("touchend",i.touchEnd)}}};d.touchSwipeListener=b}(window));(function(c){var a=c.document,d={addClass:function(f,e){f.className+=" "+e},hasClass:function(g,f){var e=new RegExp("s?"+f,"gi");return e.test(g.className)},removeClass:function(g,f){var e=new RegExp("s?"+f,"gi");g.className=g.className.replace(e,"")}},b=(function(){var e={prev:null,next:null},g={prev:null,next:null},f;return{init:function(){this.retrievePageSiblings();if(!e.prev&&!e.next){return}this.renderArows();this.syncUI()},syncUI:function(){var h=this;f=new c.touchSwipeListener({moveHandler:function(i,j){if(j){if(g[i]&&e[i]){d.hasClass(g[i],"visible")||d.addClass(g[i],"visible")}}else{d.removeClass(g.next,"visible");d.removeClass(g.prev,"visible")}},endHandler:function(i){h[i]&&h[i]()}});f.on()},retrievePageSiblings:function(){e.prev=a.querySelector("head > link[rel=prev]");e.next=a.querySelector("head > link[rel=next]")},renderArows:function(){var h=function(i){var j=a.createElement("div");j.className="spn-direction-sign "+i;a.getElementsByTagName("body")[0].appendChild(j);return j};g.next=h("next");g.prev=h("prev")},showLoadingScreen:function(){var h=a.createElement("div");h.className="spn-freezing-overlay";a.getElementsByTagName("body")[0].appendChild(h)},prev:function(){if(e.prev){this.showLoadingScreen();c.location.href=e.prev.href}},next:function(){if(e.next){this.showLoadingScreen();c.location.href=e.next.href}}}}());a.addEventListener("DOMContentLoaded",function(){a.removeEventListener("DOMContentLoaded",arguments.callee,false);try{b.init()}catch(f){alert(f)}},false)}(window));
